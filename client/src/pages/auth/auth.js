import React, { useState } from "react"
import { useNavigate } from "react-router-dom"

export default function Auth() {
  let [authMode, setAuthMode] = useState("signin")
  let [email, setEmail] = useState("")
  let [password, setPassword] = useState("")
  let [name, setName] = useState("")
  const navigate = useNavigate();

  function handleSubmitLogin(event){
    event.preventDefault()

    const userCredentials = {
        email: email,
        password: password
    }

    const requestOptions = {
      method: "POST",
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(userCredentials)
    }

    fetch("http://localhost:8090/auth/login", requestOptions)
      .then((response) => {
          return response.json()
      })
      .then((data) => {
        if(data.jwt){
          localStorage.setItem("jwt", data.jwt)
          navigate('/')
        }else{
          console.log(data.message)
        }
        
      })
      .catch(error => {
        console.log(error)
      })
  }

  async function handleSubmitRegister(event){
    event.preventDefault()

    const userCredentials = {
        name: name,
        email: email,
        password: password,
        role: 'admin'
    }

    const requestOptions = {
      method: "POST",
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(userCredentials)
    }

    fetch("http://localhost:8090/auth/register", requestOptions)
      .then((response) => {
          return response.json()
      })
      .then((data) => {
        if(data.jwt){
          localStorage.setItem("jwt", data.jwt)
          navigate('/')
        }else{
          console.log(data.message)
        }
        
      })
      .catch(error => {
        console.log(error)
      })
  }
  const changeAuthMode = () => {
    setAuthMode(authMode === "signin" ? "signup" : "signin")
  }

  if (authMode === "signin") {
    return (
      <div className="Auth-form-container">
        <form className="Auth-form">
          <div className="Auth-form-content">
            <h3 className="Auth-form-title">Sign In</h3>
            <div className="text-center">
              Not registered yet?{" "}
              <span className="link-primary" onClick={changeAuthMode}>
                Sign Up
              </span>
            </div>
            <div className="form-group mt-3">
              <label>Email address</label>
              <input
                type="email"
                className="form-control mt-1"
                placeholder="Enter email"
                onChange={e => setEmail(e.target.value)}
              />
            </div>
            <div className="form-group mt-3">
              <label>Password</label>
              <input
                type="password"
                className="form-control mt-1"
                placeholder="Enter password"
                onChange={p => setPassword(p.target.value)}
              />
            </div>
            <div className="d-grid gap-2 mt-3">
              <button type="submit" className="btn btn-primary" onClick={handleSubmitLogin}>
                Submit
              </button>
            </div>
          </div>
        </form>
      </div>
    )
  }

  return (
    <div className="Auth-form-container">
      <form className="Auth-form">
        <div className="Auth-form-content">
          <h3 className="Auth-form-title">Sign In</h3>
          <div className="text-center">
            Already registered?{" "}
            <span className="link-primary" onClick={changeAuthMode}>
              Sign In
            </span>
          </div>
          <div className="form-group mt-3">
            <label>Full Name</label>
            <input
              type="email"
              className="form-control mt-1"
              placeholder="e.g Jane Doe"
              onChange={n => setName(n.target.value)}
            />
          </div>
          <div className="form-group mt-3">
            <label>Email address</label>
            <input
              type="email"
              className="form-control mt-1"
              placeholder="Email Address"
              onChange={e => setEmail(e.target.value)}
            />
          </div>
          <div className="form-group mt-3">
            <label>Password</label>
            <input
              type="password"
              className="form-control mt-1"
              placeholder="Password"
              onChange={p => setPassword(p.target.value)}
            />
          </div>
          <div className="d-grid gap-2 mt-3">
            <button type="submit" className="btn btn-primary" onClick={handleSubmitRegister}>
              Submit
            </button>
          </div>
        </div>
      </form>
    </div>
  )
}