import React, { useEffect } from "react"
import { useNavigate } from "react-router-dom"
import CRUDTable, {
    Fields,
    Field,
    CreateForm,
    UpdateForm,
    DeleteForm
} from "react-crud-table";
import "./employee.css"

const SORTERS = {
  NUMBER_ASCENDING: mapper => (a, b) => mapper(a) - mapper(b),
  NUMBER_DESCENDING: mapper => (a, b) => mapper(b) - mapper(a),
  STRING_ASCENDING: mapper => (a, b) => mapper(a).localeCompare(mapper(b)),
  STRING_DESCENDING: mapper => (a, b) => mapper(b).localeCompare(mapper(a)),
};

const getSorter = (data) => {
  const mapper = x => x[data.field];
  let sorter = SORTERS.STRING_ASCENDING(mapper);

  if (data.field === 'id') {
    sorter = data.direction === 'ascending' ?
      SORTERS.NUMBER_ASCENDING(mapper) : SORTERS.NUMBER_DESCENDING(mapper);
  } else {
    sorter = data.direction === 'ascending' ?
      SORTERS.STRING_ASCENDING(mapper) : SORTERS.STRING_DESCENDING(mapper);
  }

  return sorter;
};


const styles = {
  container: { margin: 'auto', width: 'fit-content' },
};

export default function Employee() {    
    const navigate = useNavigate();

    async function getAllEmployees(payload){
      const requestOptions = {
        method: "GET",
      }

      let employees = await fetch("http://localhost:8090/employee", requestOptions)
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          return data.employees
        })
        .catch(error => {
          console.log(error)
        })

      employees = Array.from(employees)
      employees.sort(getSorter(payload.sort))
      return Promise.resolve(employees);
    }

    async function createEmployee(employee){
      const requestOptions = {
        method: "POST",
        headers: { 
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        },
        body: JSON.stringify(employee)
      }

      let employees = await fetch("http://localhost:8090/employee", requestOptions)
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          return data.employees
        })
        .catch(error => {
          console.log(error)
        })

      return Promise.resolve(employees);
    }

    async function deleteEmployee(employeeID){
      const requestOptions = {
        method: "DELETE",
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        }
      }

      let employees = await fetch('http://localhost:8090/employee/'+employeeID, requestOptions)
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          return data.employees
        })
        .catch(error => {
          console.log(error)
        })

      return Promise.resolve(employees);
    }

    async function updateEmployee(employee){
      const requestOptions = {
        method: "PUT",
        headers: { 
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        },
        body: JSON.stringify(employee)
      }

      let employees = await fetch("http://localhost:8090/employee/"+employee.id, requestOptions)
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          return data.employees
        })
        .catch(error => {
          console.log(error)
        })

      return Promise.resolve(employees);
    }

    useEffect(() => {
        const checkAuth = localStorage.getItem('jwt')

        if(!checkAuth){
            navigate('/auth')
        }
    }, [])

  return (
    <div style={styles.container}>
      <CRUDTable
        caption="Employees"
        fetchItems={payload => getAllEmployees(payload)}
        sortable={false}
      >
        <Fields>
          <Field
            name="id"
            label="Id"
            hideInCreateForm
            hideInUpdateForm
            readOnly
          />
          <Field
            name="name"
            label="Name"
          />
          <Field
            name="surname"
            label="Surname"
          />
          <Field
            name="position"
            label="Position"
          />
        </Fields>
        <CreateForm
          title="New Employee"
          message="Add a new employee!"
          trigger="Add employee"
          onSubmit={employee => createEmployee(employee)}
          submitText="Add"
          validate={(values) => {
            const errors = {};
            if (!values.name) {
              errors.name = 'Please, provide employee\'s name';
            }
  
            if (!values.surname) {
              errors.surname = 'Please, provide employee\'s surname';
            }
  
            if (!values.position) {
              errors.position = 'Please, provide employee\'s position';
            }

            return errors;
          }}
        />
  
        <UpdateForm
          title="Employee Update Process"
          message="Update employee"
          trigger="Update"
          onSubmit={task => updateEmployee(task)}
          submitText="Update"
          validate={(values) => {
            const errors = {};
  
            if (!values.name) {
              errors.name = 'Please, provide name';
            }
  
            if (!values.surname) {
              errors.surname = 'Please, provide employee\'s surname';
            }
  
            if (!values.position) {
              errors.position = 'Please, provide employee\'s position';
            }
  
            return errors;
          }}
        />
  
        <DeleteForm
          title="Employee Delete Process"
          message="Are you sure you want to delete the employee?"
          trigger="Delete"
          onSubmit={employee => deleteEmployee(employee.id)}
          submitText="Delete"
          validate={(values) => {
            const errors = {};
            if (!values.id) {
              errors.id = 'Please, provide id';
            }
            return errors;
          }}
        />
      </CRUDTable>
    </div>
  )
}