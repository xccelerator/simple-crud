import { Router } from "express";
import { authRoutes } from "./authRouter"
import { employeeRouter } from "./employeeRouter";

const routes = Router()

routes.use('/auth', authRoutes)
routes.use('/employee', employeeRouter)

export { routes }