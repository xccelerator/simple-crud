import { Router } from "express";
import { checkRole } from "../middleware/authMiddleware";
import { deleteEmployee, getEmployee, getEmployees, updateEmployee, addEmployee } from "../controllers/employeeControllers";

const employeeRouter = Router()

employeeRouter.post('/',checkRole(['admin']), addEmployee) // create employee
employeeRouter.get('/', getEmployees) // get all employees
employeeRouter.delete('/:id', checkRole(['admin']), deleteEmployee) // delete employee by id
employeeRouter.get('/:id', checkRole(['admin']), getEmployee) // get employee by id
employeeRouter.put('/:id', checkRole(['admin']), updateEmployee) // update employee by id

export { employeeRouter } 