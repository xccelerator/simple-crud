import { compareSync, hash } from "bcrypt"
import { sign } from "jsonwebtoken"
import { AppDataSource } from "../database/dbConnectios"
import { User } from "../database/entities/user"

function createToken(userId : string, username : string, role : string) : Promise<string>{
    return new Promise<string>((resolve, reject) => {
        sign({ userId, username, role }, process.env.JWT_SECRET_KEY, {
            expiresIn : '24h'
        }, (err, token) => {
            if(err){
                reject(err)
            }
            resolve(token)
        })
    })
}

async function checkUser(email : string) : Promise<User>{
    const userRepository = AppDataSource.getRepository(User)

    const user = await userRepository.findOne({
        where : {
            email : email
        }
    })

    return user
}

async function createUser(name : string, email : string, password : string, role : string) : Promise<User>{
    try {

        const user = await checkUser(email)

        if(user){
            throw new Error("Username already exist!")
        }

        const hashPassword = await hash(password, 7)

        const newUser = User.create({
            name : name,
            email : email,
            password : hashPassword,
            role : role
        })

        await newUser.save()

        return newUser
    } catch (error) {
        throw new Error(error.message)
    }
}

async function loginUser(email : string, password : string) : Promise<User>{
    try {
        const user = await checkUser(email)

        if(!user){
            throw new Error('Invalid account!')
        }

        let comparePassword = compareSync(password, user.password)

        if(!comparePassword){
            throw new Error('Incorect password!')
        }

        return user
    } catch (error) {
        throw new Error(error.message) 
    }
}

async function createDefaultUsers() {
    let check = await checkUser("admin")

    if(!check){
        let hashPassword = await hash("admin", 7)

        const adminUser = User.create({
            name : "admin",
            email : "admin",
            password : hashPassword,
            role : "admin"
        })
    
        await adminUser.save()
    
        hashPassword = await hash("user", 7)
        const userUser = User.create({
            name : "user",
            email : "user",
            password : hashPassword,
            role : "user"
        })
    
        await userUser.save()
    }
}
export {
    createUser,
    createToken,
    loginUser,
    createDefaultUsers,
}