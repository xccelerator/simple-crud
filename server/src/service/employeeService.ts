import { AppDataSource } from "../database/dbConnectios"
import { Employee } from "../database/entities/employee"

async function getEmployeeById(id: string) : Promise<Employee> {
    const employeeRepository = AppDataSource.getRepository(Employee)

    const employee = await employeeRepository.findOne({
        where: {
            id: id
        }
    })

    return employee
}

async function getAllEmployees() : Promise<Employee[]>{
    const employeeRepository = AppDataSource.getRepository(Employee)

    const employees = await employeeRepository.find()

    return employees
}

async function createNewEmployee(name: string, surname: string, position: string) : Promise<Employee>{
    const newEmployee = Employee.create({
        name: name,
        surname: surname,
        position: position
    })

    await newEmployee.save()

    return newEmployee
}

async function deleteById(id:string) {
    const employeeRepository = AppDataSource.getRepository(Employee)

    await employeeRepository.delete({
        id: id
    })
}

async function updateEmployeeById(id: string, updEmployee: UpdateEmployeeCredentials) {
    const employeeRepository = AppDataSource.getRepository(Employee)

    await employeeRepository.update({
        id: id
    }, {
        ...updEmployee
    })
}

export {
    getAllEmployees,
    createNewEmployee,
    deleteById,
    getEmployeeById,
    updateEmployeeById,
}