import express from "express"
import { createServer } from "http"
import { routes } from "./routes/indexRoutes"
import { AppDataSource } from "./database/dbConnectios"
import bodyParser from "body-parser";

require('dotenv').config()
import cors from "cors"
import { createDefaultUsers } from "./service/authService";

const app = express()

app.use(express.json())
app.use(cors())
app.use(routes)

app.use(bodyParser.json());

const PORT = process.env.PORT || 5000

const server = createServer(app)

async function connect(){
    let retries = 5

    while(retries){
        console.log(process.env.DATABASE_URL)
        try {
            await AppDataSource.initialize()
            createDefaultUsers()
            break;
        } catch (error) {
            console.log(error.message)
            retries -= 1
            console.log(`Retries left ${retries}`)
            await new Promise(res => setTimeout(res, 5000))
        }
    }

    if(retries){
        console.log("Database successfully connected!")

        server.listen(PORT, () => {
            console.log(`listening on: http://localhost:${PORT}`)
        })
    } else {
        console.log("Could not connect to DB!")
    }
    
}

connect();