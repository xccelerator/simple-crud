import { Entity, BaseEntity, Column, PrimaryGeneratedColumn, OneToOne, OneToMany } from "typeorm"

@Entity('user')
export class User extends BaseEntity{
    @PrimaryGeneratedColumn("uuid")
    id : string;

    @Column()
    name : string;

    @Column({
        unique: true
    })
    email : string;

    @Column()
    password : string;

    @Column({
        default : "USER",
    })
    role : string;
}