import { Entity, BaseEntity, Column, PrimaryGeneratedColumn, OneToOne, OneToMany } from "typeorm"

@Entity('employee')
export class Employee extends BaseEntity{
    @PrimaryGeneratedColumn("uuid")
    id : string;

    @Column()
    name : string;

    @Column()
    surname : string;

    @Column()
    position : string
}