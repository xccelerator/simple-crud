import { DataSource } from "typeorm";
import { User } from "./entities/user";
import { Employee } from "./entities/employee";
import dotenv from 'dotenv'; 
dotenv.config();

export const AppDataSource = new DataSource({
    type : "postgres",
    url : process.env.DATABASE_URL,
    entities : [User, Employee],
    synchronize : true,
})