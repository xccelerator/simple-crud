export {};

declare global {
  type UpdateEmployeeCredentials = {
    name: string;
    surname: string;
    position: string;
  };
}
