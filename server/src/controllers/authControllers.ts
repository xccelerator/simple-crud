import { NextFunction, Request, Response } from "express";
import { createToken, createUser, loginUser } from "../service/authService";

async function login(req : Request, res : Response, next : NextFunction) : Promise<void>{
    const { email, password } = req.body

    try {
        const user = await loginUser(email, password)

        if(!user){
            res.status(400).json({ message : "Unexpected error!" })
        }

        const jwt = await createToken(user.id,email, user.role)

        res.status(200).send({ jwt })
    } catch (error) {
        res.status(400).json({ message : error.message })
    }
}

async function register(req : Request, res : Response, next : NextFunction) : Promise<void>{
    const { name, email, password, role } = req.body

    try {
        const user = await createUser(name, email, password, role)

        if(!user){
            res.status(400).json({ message : "Unexpected error!"})
        }

        const jwt = await createToken(user.id,name, role)

        res.status(201).json({ jwt })
    } catch (error) {
        res.status(400).json({ message : error.message })
    }
}

export {
    login,
    register,
}