import { NextFunction, Request, Response } from "express";
import { createNewEmployee, deleteById, getAllEmployees, getEmployeeById, updateEmployeeById } from "../service/employeeService";


async function getEmployees(req : Request, res : Response, next : NextFunction) : Promise<void>{
    try {
        const employees = await getAllEmployees()

        res.json({employees})

        return
    } catch (error) {
        res.status(400).json({ message : error.message })
    }
}


async function deleteEmployee(req : Request, res : Response, next : NextFunction) : Promise<void>{
    try {
        const id = req.params.id

        const employee = await getEmployeeById(id)

        if(!employee){
            throw new Error("Invalid id!")
        }

        await deleteById(id);

        res.json({ message: "Employee was deleted!" })
    } catch (error) {
        res.status(400).json({ message : error.message })
    }
}

async function getEmployee(req : Request, res : Response, next : NextFunction) : Promise<void>{
    try {
        const id = req.params.id

        const employee = await getEmployeeById(id)

        if(!employee){
            throw new Error("Invalid id!")
        }

        res.json({ employee })
    } catch (error) {
        res.status(400).json({ message : error.message })
    }
}

async function updateEmployee(req : Request, res : Response, next : NextFunction) : Promise<void>{
    try {
        const id = req.params.id

        const employee = await getEmployeeById(id)

        if(!employee){
            throw new Error("Invalid id!")
        }

        const updEmployee: UpdateEmployeeCredentials = req.body

        await updateEmployeeById(id, updEmployee)

        res.json({ message: "Employee was updated!" })
    } catch (error) {
        res.status(400).json({ message : error.message })
    }
}

async function addEmployee(req : Request, res : Response, next : NextFunction) : Promise<void>{
    try {
        const { name, surname, position } = req.body

        await createNewEmployee(name, surname, position)

        res.json({ message: "Employee was created succesfully!" })
    } catch (error) {
        res.status(400).json({ message : error.message })
    }
}

export {
    getEmployees,
    deleteEmployee,
    getEmployee,
    updateEmployee,
    addEmployee,
}