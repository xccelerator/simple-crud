Simple CRUD application using React and nodejs
#### Start project
```bash
docker-compose up
```

### Endpoints
* page for CRUD (visible after login)
**http://localhost:3000/** 
* page for authorization
**http://localhost:3000/auth** 


#### Default admin
```json
email: admin
password: admin
```

#### Default user
```json
email: user
password: user
```

#### Admin functionality
* Create
* Delete
* Update
* Read

#### User functionality
* Read